<?php

function commerce_yescredit_regions() {
  return array(
   'MOW' => 'Москва',
   'MOS' => 'Московская область',
   'SPE' => 'Санкт-Петербург',
   'LEN' => 'Ленинградская область',
   'AD' => 'Адыгея',
   'ALT' => 'Алтайский край',
   'AMU' => 'Амурская область',
   'ARK' => 'Архангельская область',
   'AST' => 'Астраханская область',
   'BA' => 'Башкирия',
   'BEL' => 'Белгородская область',
   'BRY' => 'Брянская область',
   'BU' => 'Бурятия',
   'VLA' => 'Владимирская область',
   'VGG' => 'Волгоградская область',
   'VLG' => 'Вологодская область',
   'VOR' => 'Воронежская область',
   'DA' => 'Дагестан',
   'YEV' => 'Еврейская автономная область',
   'ZAB' => 'Забайкальский край',
   'IVA' => 'Ивановская область',
   'IN' => 'Ингушетия',
   'IRK' => 'Иркутская область',
   'KB' => 'Кабардино-Балкария',
   'KGD' => 'Калининградская область',
   'KL' => 'Калмыкия',
   'KLU' => 'Калужская область',
   'KAM' => 'Камчатский край',
   'KC' => 'Карачаево-Черкессия',
   'KR' => 'Карелия',
   'KEM' => 'Кемеровская область',
   'KIR' => 'Кировская область',
   'KO' => 'Коми',
   'KOS' => 'Костромская область',
   'KDA' => 'Краснодарский край',
   'KYA' => 'Красноярский край',
   'KGN' => 'Курганская область',
   'KRS' => 'Курская область',
   'LIP' => 'Липецкая область',
   'MAG' => 'Магаданская область',
   'ME' => 'Марий Эл',
   'MO' => 'Мордовия',   
   'MUR' => 'Мурманская область',
   'NEN' => 'Ненецкий автономный округ',
   'NIZ' => 'Нижегородская область',
   'NGR' => 'Новгородская область',
   'NVS' => 'Новосибирская область',
   'OMS' => 'Омская область',
   'ORE' => 'Оренбургская область',
   'ORL' => 'Орловская область',
   'PNZ' => 'Пензенская область',
   'PER' => 'Пермский край',
   'PRI' => 'Приморский край',
   'PSK' => 'Псковская область',
   'AL' => 'Республика Алтай',
   'ROS' => 'Ростовская область',
   'RYA' => 'Рязанская область',
   'SAM' => 'Самарская область',
   'SAR' => 'Саратовская область',
   'SAK' => 'Сахалинская область',
   'SVE' => 'Свердловская область',
   'SE' => 'Северная Осетия',
   'SMO' => 'Смоленская область',
   'STA' => 'Ставропольский край',
   'TAM' => 'Тамбовская область',
   'TA' => 'Татарстан',
   'TVE' => 'Тверская область',
   'TOM' => 'Томская область',
   'TY' => 'Тува',
   'TUL' => 'Тульская область',
   'TYU' => 'Тюменская область',
   'UD' => 'Удмуртия',
   'ULY' => 'Ульяновская область',
   'KHA' => 'Хабаровский край',
   'KK' => 'Хакасия',
   'KHM' => 'Ханты-Мансийский автономный округ',
   'CHE' => 'Челябинская область',
   'CE' => 'Чечня',
   'CU' => 'Чувашия',
   'CHU' => 'Чукотский автономный округ',
   'SA' => 'Якутия',
   'YAN' => 'Ямало-Ненецкий автономный округ',
   'YAR' => 'Ярославская область'
   );
 }
 
/**
 * Verifies that $phonenumber is a valid ten-digit Russian phone number
 *
 * @param string $phonenumber
 * @return boolean Returns boolean FALSE if the phone number is not valid.
 */
function commerce_yescredit_valid_ru_phone_number($phonenumber) {

  //$phonenumber = trim($phonenumber);

  // define regular expression
  $regex = "/
    \D*           # ignore non-digits
    [78]?         # an optional 78
    \D*           # optional separator
    \d{3,5}       # area code 3-5 digit
    \D*           # optional separator
    \d{1,3}       # 3-digit prefix
    \D*           # optional separator
    \d{2}         # 2-digit line number
    \D*           # optional separator
    \d{2}         # 2-digit line number
    \D*           # ignore trailing non-digits
    /x";
  // return true if valid, false otherwise
  return (bool) preg_match($regex, $phonenumber);
}

/**
 * Convert a valid Russian phone number into standard +7 (495) 567-53-09 or +7 (444xx) 67-53-09 or mobile 8 910 414-56-90 format
 *
 * @param $phonenumber must be a valid ten-digit number (with optional extension)
 *
 */
function commerce_yescredit_format_ru_phone_number($phonenumber, $field = FALSE) {

  // define regular expression
  $regex = "/
    ^\D*          # ignore non-digits
    ([78])?       # an optional 78
    \D*           # optional separator
    (\d{3,5})  	  # area code 3-5 digit
    \D*           # optional separator
    (\d{1,3})  	  # capture 3-digit prefix
    \D*           # optional separator
    (\d{2})       # 2-digit line number
    \D*           # optional separator
    (\d{2})       # 2-digit line number
    \D*           # ignore trailing non-digits
  /x";

  // get digits of phone number
  preg_match($regex, $phonenumber, $matches);

  // construct ten-digit phone number
  $phonenumber = $matches[1] . ' (' . $matches[2] . ') ' . $matches[3] . ' - ' . $matches[4] . ' - ' . $matches[5];

  return $phonenumber;
}
